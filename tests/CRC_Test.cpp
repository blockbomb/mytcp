#include <iostream>
#include <string.h>
#include "CRC.h"

using namespace std;


int main ()
{
    
    // const char* x = "7777777777";
    // unsigned char* data;
    // data = (unsigned char*) x;

    unsigned char data[1000];
    memset(data, 0, 1000);

    cout << "Please enter data: ";
    cin >> data;
    cout << "The value you entered is " << data << "\n";

    unsigned short rem;
    rem = mytcp::util::crc_generate(data, 1000, 2);
    int len = strlen((char*)data);
    cout << "Remainder: " << hex << (unsigned short) rem << "\n";
    rem = mytcp::util::crc_check(data, len, rem, 2);
    cout << "Check: " << hex<< (unsigned short) rem << "\n";

    return 0;
}

