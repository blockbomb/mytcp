//
// File: test_timer.cpp
// Author: Brian Block
//
// The test_timer program was created to run
// simple tests for the timer process.
//

#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netdb.h>
#include <errno.h>
#include <time.h>

#include "constants.h"
#include "types.h"

using namespace std;

// Global Variables
int timerComm, localComm;
char* sTimerPort, *sLocalPort;
bool wait = false;

char buf[MAX_PACKET_SIZE];

fd_set readfds;
struct timeval tv;

void print_usage(char* progName)
{
  cout << progName << " <local-port> <timer-process-port>\n"
       << "  local-port         - the port to listen for messages\n"
       << "  timer-process-port - the port the timer process is\n"
       << "                       waiting for comm on.\n";
}

void init_sockets()
{
  int status;
  struct addrinfo hints;
  struct addrinfo *timerCommInfo, *localCommInfo;

  memset (buf, 0, MAX_PACKET_SIZE);
  memset (&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  // Setup local communications
  if ( (status = getaddrinfo(NULL,
                             sTimerPort,
                             &hints,
                             &timerCommInfo)) ) {
    cerr << "getaddrinfo (timer-comm): "
         << gai_strerror(status) << endl;
    exit(1);
  }

  if ( (status = getaddrinfo(NULL,
                             sLocalPort,
                             &hints,
                             &localCommInfo)) ) {
    cerr << "getaddrinfo (local-comm): "
         << gai_strerror(status) << endl;
    exit(1);
  }
  // Open Socket for communication
  timerComm = socket(timerCommInfo->ai_family,
                     timerCommInfo->ai_socktype,
                     timerCommInfo->ai_protocol);

  localComm = socket(localCommInfo->ai_family,
                     localCommInfo->ai_socktype,
                     localCommInfo->ai_protocol);

  // Bind
  if ( (status = bind(localComm,
                      localCommInfo->ai_addr,
                      localCommInfo->ai_addrlen)) ) {
    cerr << "bind (local-comm): "
         << strerror(errno) << endl;
    exit(1);
  }

  // Connect
  if ( (status = connect(timerComm,
                         timerCommInfo->ai_addr,
                         timerCommInfo->ai_addrlen)) ) {
    cerr << "connect (timer-comm): " << strerror(errno);
    exit(2);
  }
}

// Sent message to add timer to the deltal list
// size of float is 4 bytes
void add_timer(float time, unsigned int sn)
{
  ssize_t size;
  timer_packet tp;
  memset(&tp, 0, sizeof(tp));
  tp.command = 'a';
  memcpy(&tp.msg[4], &time, sizeof(float));
  memcpy(&tp.msg[0], &sn, sizeof(unsigned int));

  if ( (size = send ( timerComm, &tp, sizeof(tp), 0)) < 0 ) {
    cerr << "send (timer-comm): " <<  strerror(errno);
  }
}

void cancel_timer(unsigned int sn)
{
  ssize_t size;
  timer_packet tp;
  memset( &tp, 0, sizeof(tp) );
  tp.command = 'c';
  memcpy(&tp.msg[0], &sn, sizeof(unsigned int));

  if ( (size = send ( timerComm, &tp, sizeof(tp), 0)) < 0 ) {
    cerr << "send (timer-comm): " <<  strerror(errno);
  }
}

void process_command(char* command, int size)
{
  string op1, op2, op3;
  stringstream ss(command);
  ss >> op1;
  ss >> op2;
  ss >> op3;
  switch ( op1.c_str()[0] ) {
  case 'a':
    add_timer(atof(op2.c_str()), atoi(op3.c_str()));
    break;
  case 'c':
    cout << "Cancel timer..." << endl;
    cancel_timer(atoi(op2.c_str()));
    break;
  case 'w':
    cout << "waiting..." << endl;
    wait = true;
  default:
    // ignore command
    break;
  }
}

//
// The build_select helper initalizes the sets
// used with the select function and timeouts
//
// return:
//   the max file descriptor + 1.
int build_select()
{
  FD_ZERO(&readfds);

  FD_SET( localComm, &readfds);

  tv.tv_sec = 0;
  tv.tv_usec = 50000;
  return ( localComm + 1 );
}

int main (int argc, char** argv)
{
  if (argc < 3) {
    print_usage(argv[0]);
    exit(1);
  }
  sLocalPort = argv[1];
  sTimerPort = argv[2];
  init_sockets();
  char input[256];
  int hits, maxfd;
  while ( 1 ) {
    maxfd = build_select();
    if ( (hits = select(maxfd, &readfds, NULL, NULL, &tv)) < 0 ) {
      cerr << "select: " << strerror(errno) << endl;
    } else {
      ssize_t size;
      if ( hits > 0 ) {
        if ( (size = recv(localComm,
                          buf,
                          MAX_PACKET_SIZE,
                          0)) < 0 ) {
          cerr << "recv (timer-comm): "
               << strerror(errno) << endl;
        }
        if (size >= (int) sizeof(timer_packet) ) {
          timer_packet tp;
          memset(&tp, 0, sizeof(tp));
          memcpy(&tp, buf, size);
          unsigned int seqNum;
          memcpy(&seqNum, &tp.msg[0], sizeof(unsigned int));
          cout << "timer expired for sequence number ( " << seqNum  << " )" <<  endl;
        }
      } else {
        if ( !wait ) {
          cout << "Commands to send:\n"
               << "  'a <ttl> <seqNum>' - add a timer with ttl, and sequcence number\n"
               << "  'c <seqNum>'          - cancel a timer with by sequence number\n"
               << "  'w'                   - stop interactive mode and wait\n"
               << "                          must exit app after this command\n"
               << "                          with <ctrl>-c." << endl
               << "Please enter a command to send to the timer process: ";
          cin.getline(input, 256);
          process_command(input, 256);
        }
      }
    }
  }
  return 0;
}
