/*
This is simple program to receive packets from MyTCPs.
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
using namespace std;

#define BUFFER_SIZE 1000

struct hostent *gethostbyname();

int main(int argc, char *argv[]) {
  if (argc != 3) {
    cout <<"PROGRAM <MyTCPs_machine> <MyTCPs_port>"<<endl;
    exit(1);
  }
  
  struct sockaddr_in daemon_addr;
  int daemon_sock;
  struct hostent *hp;
  char cli_buf[BUFFER_SIZE] = "";
  
  /* create socket for connecting to MyTCPs */
  daemon_sock = socket(AF_INET, SOCK_DGRAM,0);
  /* construct name for connecting to server */
  daemon_addr.sin_family = AF_INET;
  daemon_addr.sin_port = htons(atoi(argv[2]));
  /* convert hostname to IP address and enter into name */
  daemon_addr.sin_addr.s_addr = INADDR_ANY;
  
  if(bind(daemon_sock, (struct sockaddr *)&daemon_addr, sizeof(daemon_addr)) < 0) {
    perror("getting socket name");
    exit(2);
  }
  
  while(1) {
    bzero(cli_buf, BUFFER_SIZE);
    if(recv(daemon_sock, cli_buf, sizeof(cli_buf), 0) < 0) {
      perror("receiving datagram message");
      close(daemon_sock);
      exit(2);
    }
    printf("test_mytcps receives: %s\n",cli_buf);
  }
  
  close(daemon_sock);
  exit(3);  
}