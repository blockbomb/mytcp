/*
*  Simple program that repeatedly sends UDP packets to the Troll
*/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
using namespace std;

#define BUFFER_SIZE 1000

struct sockaddr_in troll;
struct hostent *troll_machine, *remote_machine, *gethostbyname();

typedef struct {
  struct sockaddr_in header;
  char body[BUFFER_SIZE];
  int msgSize;
} troll_packet;

int main(int argc, char *argv[])
{
  if (argc != 6) {
    cout <<"PROGRAM <troll_machine> <troll_port> <num_messages to send> <remote_machine> <remote_port>"<<endl;
    exit(1);
  }
  
  troll_machine = gethostbyname(argv[1]);
  if (troll_machine < 0) {perror("error unknown troll host");}
  remote_machine = gethostbyname(argv[4]);
  if (remote_machine < 0) {perror("error unknown remote host");}
  int sock = socket(AF_INET, SOCK_DGRAM,0);
  if (sock < 0) {perror("opening datagram socket");}
  int num_messages = atoi(argv[3]);
  troll_packet message;
  int result = 0;
  message.header.sin_family = htons(AF_INET);
  bcopy((char *)remote_machine->h_addr, (char *)&message.header.sin_addr, remote_machine->h_length); /* Internet address of remote machine */
  message.header.sin_port = htons(atoi(argv[5])); /* remote port */  troll.sin_family = AF_INET;  bcopy((char *)troll_machine->h_addr, (char *)&troll.sin_addr, troll_machine->h_length); /* Internet address of machine running the troll */
  troll.sin_port = htons(atoi(argv[2])); /* troll port */
    /* fill buffer with data */
  char sample_buf[BUFFER_SIZE] = "Hello in UDP from client";
  bcopy(sample_buf, message.body, sizeof(message.body));
  message.msgSize = sizeof(message.body);
    
  while(num_messages > 0) {    result = sendto(sock, (char *)&message.body, sizeof message.body, 0, (struct sockaddr *)&troll, sizeof troll);
    if (result < 0) {perror("error sending message");}
    cout <<num_messages<<": "<<message.body<<endl;
    num_messages--;
  }
}
