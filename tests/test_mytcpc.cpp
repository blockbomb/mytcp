//
// File: test_mytcp.cpp
// Author: Brian Block
//
// test_mytcp was a simple test application to simulate the interaction between ftpc
// and MyTCPc.  

#include <iostream>
#include <errno.h>
#include <cstring>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>

#include "constants.h"
#include "types.h"

using namespace std;

int mytcpSock, ftpcSock;
void init() {
  struct addrinfo hints;
  struct addrinfo *MyTCPcSendInfo, *ftpcInfo;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  int status;
  if ( (status = getaddrinfo(NULL, MY_TCP_FTP_LISTEN_PORT,
                             &hints,
                             &MyTCPcSendInfo)) ) {
    cerr << "getaddrinfo (mytcp-comm): "
         << gai_strerror(status) << endl;
  }

  if ( (status = getaddrinfo(NULL, FTPC_PORT,
                             &hints,
                             &ftpcInfo)) ) {
    cerr << "getaddrinfo ( ftpcInfo ): "
         << gai_strerror(status) << endl;
  }

  mytcpSock = socket(MyTCPcSendInfo->ai_family,
                         MyTCPcSendInfo->ai_socktype,
                         MyTCPcSendInfo->ai_protocol);

  ftpcSock = socket(ftpcInfo->ai_family,
                    ftpcInfo->ai_socktype,
                    ftpcInfo->ai_protocol);

  if ( (status = bind ( ftpcSock,
                      ftpcInfo->ai_addr,
                      ftpcInfo->ai_addrlen)) ) {
    cerr << "bind ( ftcpInfo ): "
         << strerror(errno) << endl;
  }
  if ( (status = connect(mytcpSock,
                         MyTCPcSendInfo->ai_addr,
                         MyTCPcSendInfo->ai_addrlen)) ) {
   cerr << "connect (mytcp-comm): "
        << strerror(errno) << endl; 
  }
}

int main ( int argc, char** argv ) {
  init();
  char buf[100];
  ftp_packet pkt;
  memset(&pkt, 0, sizeof(pkt));
  memset(buf, 1, 100);
  memcpy(pkt.msg, buf, 100);
  pkt.header.command = 'd';
  
  send (mytcpSock, &pkt, sizeof(pkt.header) + 100, 0);
  recv (ftpcSock, buf, 100, 0);

  memcpy(&pkt, buf, sizeof(pkt.header));
  cout << pkt.header.bytes_available;
  return 0;
}
