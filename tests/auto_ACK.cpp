//
// File:
// Author: Brian Block
// 
// auto_Ack was created as a testing device for the client side.
// it is ment to simulate the server side by acking every data packet.
//
#include <iostream>
#include <errno.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "constants.h"
#include "types.h"
#include "CRC.h"

using namespace std;

int trollSendSock, trollReceiveSock;
char *sTrollHost;
char *sTrollPort;

struct sockaddr_in forwardAddress;

void init() {
  struct addrinfo hints;
  struct addrinfo *trollSendInfo, *trollReceiveInfo;
  struct addrinfo *ftpcInfo;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  int status;
  if ( (status = getaddrinfo(sTrollHost, 
                             sTrollPort,
                             &hints,
                             &trollSendInfo)) ) {
    cerr << "getaddrinfo (troll-send): "
         << gai_strerror(status) << endl;
  }

  if ( (status = getaddrinfo(NULL, MY_TCP_TROLL_LISTEN_PORT,
                             &hints,
                             &trollReceiveInfo)) ) {
    cerr << "getaddrinfo (troll-receive): "
         << gai_strerror(status) << endl;
  }

  if ( (status = getaddrinfo(DEFAULT_FTPC_HOST,
                             MY_TCP_TROLL_LISTEN_PORT,
                             &hints,
                             &ftpcInfo)) ) {
    cerr << "getaddrinfo (ftpc-info): "
         << gai_strerror(status) << endl;
  }


  trollSendSock = socket(trollSendInfo->ai_family,
                         trollSendInfo->ai_socktype,
                         trollSendInfo->ai_protocol);
 
  trollReceiveSock = socket ( trollReceiveInfo->ai_family,
                              trollReceiveInfo->ai_socktype,
                              trollReceiveInfo->ai_protocol);

  if ( (status = bind ( trollReceiveSock,
                        trollReceiveInfo->ai_addr,
                        trollReceiveInfo->ai_addrlen)) ){
    cerr << "bind ( troll_receive ): "
         << strerror(errno);
    exit ( 1 );
  }

  if ( (status = connect(trollSendSock,
                         trollSendInfo->ai_addr,
                         trollSendInfo->ai_addrlen)) ) {
   cerr << "connect (mytcp-comm): "
        << strerror(errno) << endl; 
   exit ( 1 );
  }

  memcpy(&forwardAddress, (struct sockaddr_in*) ftpcInfo->ai_addr , sizeof (forwardAddress) );
}

int main ( int argc, char** argv ) {
  if ( argc < 3 ) {
    cout << "PROGRAM <troll-host> <troll-port>\n";
    exit(0);
  } 
  sTrollHost = argv[1];
  sTrollPort = argv[2];
  init();

  char *buf[MAX_PACKET_SIZE];
  troll_packet tPkt;
  
  bool sendFin = false;
  while ( 1 ) {
    recv ( trollReceiveSock, buf, MAX_PACKET_SIZE,0 );
    memcpy ( &tPkt, buf, sizeof(troll_packet));
    if ( ((mytcp_packet*)tPkt.msg)->header.flags && FIN_BIT ){
      sendFin = true;
    }
    // Replace forward address, ACK flag, and ack number    
    // tPkt.header.sin_addr = forwardAddress.sin_addr;
    tPkt.header.sin_port = forwardAddress.sin_port; 
    ((mytcp_packet*)tPkt.msg)->header.flags |= ACK_BIT;
    ((mytcp_packet*)tPkt.msg)->header.checksum = 0x0000;
    ((mytcp_packet*)tPkt.msg)->header.ackNum = ((mytcp_packet*)tPkt.msg)->header.seqNum;

    if ( tPkt.msgSize >= 0 && tPkt.msgSize <= MAX_TCP_PACKET_SIZE ) {

      unsigned short crc = mytcp::util::crc_generate((unsigned char*)tPkt.msg, tPkt.msgSize, 2);
      ((mytcp_packet*)tPkt.msg)->header.checksum = crc;

      send ( trollSendSock, &tPkt, sizeof(troll_packet), 0 );
      if ( sendFin ) {
        memset ( tPkt.msg, 0, sizeof (mytcp_header) );
        ((mytcp_packet*)tPkt.msg)->header.flags = FIN_BIT;
        tPkt.msgSize = sizeof(mytcp_header);
        send ( trollSendSock, &tPkt, sizeof(troll_packet), 0);
        recv ( trollReceiveSock, &tPkt, sizeof(troll_packet), 0);
        if ( ((mytcp_packet*)tPkt.msg)->header.flags && (FIN_BIT | ACK_BIT) ) {
          break;
        }
      }
    }
    usleep(100);
  }
  return 0;
}
