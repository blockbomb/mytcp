//
// Author: Brian Block
// Data: May 17th 2012
//
// The wrap around buffer will be used by the MyTCP// process to store data recieved 
// from ftpc until moved into the sending window.
//
#ifndef WRAP_AROUND_BUFFER_H
#define WRAP_AROUND_BUFFER_H

#include "constants.h"

namespace mytcp 
{
  namespace util 
  {
    class WrapAroundBuffer 
    {
      public:
        WrapAroundBuffer();
        ~WrapAroundBuffer();

        //
        // write_data add's the content of buf into
        // the wrap around buffer. 
        //
        // return:
        //   on successful addition it returns
        //   the number of bytes added to the wrap
        //   around buffer. On a failure it
        //   returns -1
        //
        // params: 
        //   buf - buffer containg data to add
        //         to wrap around buffer.
        //   len - length of buf.
        //
        int write_data(char const* buf, int len);

        //
        // read_data fills the buffer passed in 
        // with as much data as possible.
        //
        // return
        //   returns the number of bytes
        //   filled into the buffer
        //
        // params:
        //   buf - buffer to be filled with
        //         data
        //   len - length of buf.
        //
        int read_data(char* buf, int len);

        // 
        // used is used to get the current # of 
        // bytes used in the buffer.
        //
        // return:
        //   the number of bytes available for read
        //   in the buffer.
        //
        int used();

        // 
        // unused is used to get the current # of 
        // bytes not used in the buffer.
        //
        // return:
        //   the number of bytes available for
        //   writing in the buffer.
        //
        int unused();

        //
        // size is the total size of the buffer
        //
        // return:
        //   the total space avaliable in the buffer
        //   in bytes.
        int size();
      private:
        char buffer[WRAP_AROUND_BUFFER_SIZE];
        int dataStart;
        int count;
    };
  }
}

#endif
