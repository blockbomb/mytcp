// 
// File: types.h
// Author: Brian Block
//
// these types are used to represent our packet structure used to send information
// throughout the application.
//
#ifndef TYPES_H
#define TYPES_H

#include "constants.h"
#include <sys/types.h>

// 
// Troll Packet type is used to encapsulate mytcp_packet containting
// information required for use with the troll application
//
typedef struct {
  struct sockaddr_in header;
  char msg[MAX_TCP_PACKET_SIZE];
  int msgSize;
} troll_packet;

//
// The ftp_header is used withing ftp_packet to decouple header information
// with data.
//
typedef struct { 
  char command;
  int bytes_available;
} ftp_header;

//
// The ftp_packet contains the header information with data
//
typedef struct {
  ftp_header header;
  char msg[MAX_MSG_SIZE];
} ftp_packet;

//
// The timer packet us used to communicate with the timer process
//
typedef struct {
  char command;
  char msg[TIMER_MSG_SIZE];
} timer_packet;

//
// mytcp_header contains information usedby the MyTCP applications
// to provide TCP like functionality.
//
typedef struct {
  short srcPort;
  short destPort;
  int seqNum;
  int ackNum;
  short windowSize;
  short checksum;
  char flags;
} mytcp_header;

//
// the mytcp_packet contains the header and data
//
typedef struct {
  mytcp_header header;
  char msg[MAX_MSG_SIZE];
} mytcp_packet;

#endif
