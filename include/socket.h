//
// File: socket.h
// Author: Brian Block
//
// This header includes all wrap socket wrapper calls
// implemented as the uppercase equivilant of their
// respective system calls.
//
#include <sys/types.h>
#include <sys/socket.h>

namespace mytcp
{
  //
  //  The SOCKET function is used to create a
  //  A socket and register it with the operating
  //  system.
  // 
  //  return:
  //    file descriptor associating socket with
  //    type and protocol.
  //
  int SOCKET(int domain, int type, int protocol);

  //
  // the BIND function is used to register a local port
  // for communications.
  //
  // return:
  //   0 - on success
  //  -1 - on failure
  //
  int BIND(int sockfd, const struct sockaddr *my_addr,
           socklen_t adderlen);
  //
  //  the ACCEPT function is used to accept a connection
  //  on a previously bound socket.
  //
  //  return:
  //   non-negative - on success
  //   -1           - on failure
  // 
  int ACCEPT(int sockfd, struct sockaddr *addr,
             socklen_t adderlen);

  //
  // the CONNECT function initaites a connection on
  // on a socket.
  //
  // return:
  //   0 - on success
  //  -1 - on failure
  //
  int CONNECT(int sockfd, struct sockaddr *addr, 
              socklen_t adderlen);

  //
  //  the SEND function sends a message on a socket.
  // 
  //  return:
  //    number of bytes sent - on success
  //    -1                   - on failure
  int SEND(int sockfd, const void *buf, size_t len,
           int flags);

  //
  //  the RECV function receives a message on a socket.
  //
  //  return:
  //    0                       - connecton was properly shutdown
  //   -1                       - on failure
  //   number of bytes recieved - on success
  //
  int RECV(int s, void *buf, size_t len, int flags);

  //
  //  the CLOSE function closes a socket by disabling the file
  //  descriptor
  //
  //  return:
  //    0 - on success
  //   -1 - on failure
  //
  int CLOSE(int sockfd);
}
