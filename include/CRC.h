//
// File: CRC.h
// Author: Brian Block
//
// This file contains utility functions to calculate the CRC.
//
#define CRC_16 0x8005

namespace mytcp 
{
  namespace util
  {
  //
  // crc_check performs the CRC calculation on the provided data.
  // return:
  //   true  - if CRC check passes
  //   false - if CRC check detects error
  //
  // parameters:
  //   data - the data to perform the check on.
  //   dLen - the length of the data.
  //   rLen - the length of the checksum in bytes.
  //
   bool crc_check(unsigned char* const data, int dLen, unsigned short remainder, int rLen);
      
  //
  // crc_generate performs the CRC calculation and returns 
  // calculated R bit sequence
  //
  // return:
  //   a pointer to a byte array containing the remainder R
  //
  // parameters:
  //   data - the data to perform the CRC generation on.
  //   dLen - the length of the data in bytes.
  //   rLen - the length of the checksum in bytes.
  //
  unsigned short crc_generate(unsigned char* const data, int dLen, int rLen);


  //
  // crc_algorithm performs a simple implementation of crc.
  //
  // parameters:
  //   data - the datat to perform the check on
  //   dlen - the length of the data (in bytes)
  //   remainder - the initial remainder to start the algorithm with
  //   rlen - the remainder length ( currently not used 2 should be passed in )
  // return:
  //   an unsigned short representing the remainder. 
  unsigned short crc_algorithm(unsigned char* const data, int dLen, unsigned short remainder, int rLen);
      
  }

}
