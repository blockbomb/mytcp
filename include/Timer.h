//
// File: Timer.h
// Author: Brian Block
//
// The timer class is designed to maintain 
// the ammount of left and to notify when the
// time has expired. 
// 
#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>
#include <sys/types.h>

namespace mytcp {
  namespace util {
    class Timer {
      public:
        //
        // The Constructor for a timer takes in an
        // ammount of time, in seconds, until the timer
        // expires.
        //
        Timer(double ttl, unsigned int sn);
        ~Timer();
        
        //
        // enable() signifies that the timer is active
        // and the tick() call will decrement it's ttl
        // 
        // Return:
        //   - true - on success
        //   - false - on failure
        // 
        bool activate();
  
        //
        // deactivate() signifies that the timer is 
        // not active and tick() will be a null function.
        // 
        // Return:
        //   - true - on success
        //   - false - on failure
        //
        bool deactivate();

        //
        // Tick() will decrement the timer by the
        // ammount of time since the last tick call.
        // if the time remaining in the timer is
        // less than or equal to 0 the timer will
        // expire returning true.  the first tick call
        // will measure the time from when then timer was
        // enabled.  if the timer is not active the tick
        // call will do nothing.
        //
        // Return
        //   false - if the timer has not expired.
        //   true  - if the timer has expired.
        ///
        bool tick();

        //
        // time_left
        //
        double time_till_expire () const;

        //
        // Sets the ttl of the timer.
        // 
        // Returns:
        //   timer the timer was set to
        //
        double set_timer(double);

        //
        // returns the sequence number associated with timer.
        //
        unsigned int get_sequence_number();

        //
        // Operator Overloading for comparisons
        //
        bool operator< (const Timer &rhs) const;
        bool operator> (const Timer &rhs) const;

        //
        // returns a formated string for the timer.
        //
        const char* pretty_print();

      private:
        double timeLeft; 
        int seqNum;
        bool active;
        timeval timeStamp;
    };
  }
}
#endif
