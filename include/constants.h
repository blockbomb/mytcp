//
// File: constants.h
// AUthor: Brian Block
//
// This file contains all the magic numbers used throughout the application
// I know that #defines are frowned upon because they don't show up in the 
// debuggin symbols but we live and learn.
//
#ifndef CONSTANTS_H
#define CONSTANTS_H

#define MAX_PACKET_SIZE 1000
#define MAX_TCP_PACKET_SIZE 920
#define MAX_MSG_SIZE 900
#define MAX_SEQ_NUMB 64
#define TIMER_MSG_SIZE 8
#define WRAP_AROUND_BUFFER_SIZE 30000
#define WINDOW_SIZE 20
#define ALPHA 0.125
#define BETA 0.25
#define U 4
#define MAX_RTO 6
#define STARTING_SEQ_NUM 0

#define FIN_BIT 0x01
#define ACK_BIT 0x10
#define SYN_BIT 0x02

#define FTPS_PORT "6783"
#define FTPC_PORT "6787"
#define TIMER_PORT "6788"
#define MY_TCP_TROLL_LISTEN_PORT "6786"
#define MY_TCP_TIMER_LISTEN_PORT "6789"
#define MY_TCP_FTP_LISTEN_PORT "6790"

#define DAEMON_PORT_NUM 6784

#define DEFAULT_FTPS_HOST "delta"
#define DEFAULT_FTPC_HOST "epsilon"
#endif
