MyTCP was created as an internetworking class project
The goal of the project is to implement a TCP like
service using UDP.  The project proposal can be found
int the doc directory for more information about the 
specifics of the project. 

Team Members:
  Brian Block
  Mohammed Shareef

code repository with history:
www.bitbucket.org/blockbomb/mytcp

-project_root
 |-include  (headers, constants, and data types)
 |-src      (source files)
 |-tests    (source files for tests)
 |-dep      (dependencies aka. troll)
 |-doc      (Documents for the project)
 |-build    (binary outputs generated after make)

To Build:
  Change directory into the src folder and run the
  make file by typeing make.  This will build all
  targets the output of this build can be found 
  under $(ProjectDir)/build/$(Platform)/bin/

  NOTE: our application was built on the linux machines
  though they should build on the unix machines it was
  not tested on them. 

To Run:
  Take a look at the 'include/constants.h' file, this file
  contains all the constansts required for the application
  to run. the client and server must run on the DEFAULT_FTPC_HOST
  and DEFAULT_FTPS_HOST respectivly.  if you change this file 
  the whole project will be required to build again. 

  in the constants file almost all of the ports are defined for
  communications except the troll port.  this can is specified
  when starting the MyTCPc or MyTCPs application.  

  Setting Up.
    1) ssh to the DEFAULT_FTPS_HOST in 3 terminals
    2) ssh to the DEFAULT_FTPC_HOST in 2 terminals
    3) ssh to the host you wish to run the troll on (eg. "mu")
    4) set up troll
       * run the command './troll 6785' and set any options you choose
    5) Set up client on DEFAULT_FTPC_HOST
       * in one terminal run the command 'MyTCPc <troll-host> <troll-port>'
         in our example "MyTCPc mu 6785"
       * in another terminal run the command './timer 6788 6789' to start
         the timer application.
    6) Set up the server on DEFAULT_FTPS_HOST
       * run the command 'MyTCPs <troll-host> <troll-port>' 
         in our example "./MyTCPs mu 6785"
       * start the ftps server './ftps <local-port>'
         in our example "./ftps 6783"
    7) Once the server/troll/and client set up time to issue the
       ftpc command.  in the last terminal of the client host run the command
       './ftpc <file-to-transfer>'
