//
// File: timer.cpp
// Author: Brian Block
//
// The responsibility of the timer application is to maintain
// a  queue of delta timers associated with a sequence number
// and notify the TCPDc when a timer has expired.
//
#include <iostream>
#include <list>
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netdb.h>

#include "constants.h"
#include "types.h"
#include "Timer.h"

using namespace std;
using namespace mytcp::util;

// Global Varialbes
//
int localComm, myTcpSock;
char *sLocalPort, *sMytcpPort;
char buf[MAX_PACKET_SIZE];

fd_set readfds;
struct timeval tv;

list<Timer*> deltaList;
//
//
//
void print_usage(char* ProgName){
  cout << ProgName << " <local-comm-port> <MyTCPc-port>\n"
       << "    <local-comm-port> - the well known port to run the timer server on.\n"
       << "    <MyTCPc-port>     - the local port MyTCPc is running on.\n";
}

// Compare function to order the delta list.
bool compare_deltaList(Timer* first, Timer* second) {
  bool ans = *first < *second;
  if ( !ans )
    first->deactivate();
  return *first < *second;
}

// Drecment the deltaList
void decrement_timer() {
    if ( !deltaList.empty() ) {
      //cout << deltaList.front()->time_till_expire() << endl;
      if ( deltaList.front()->tick() ) {
        timer_packet tp;
        ssize_t size = 0;
        unsigned int seqNum = deltaList.front()->get_sequence_number();
        memset(&tp, 0, sizeof(tp));
        tp.command = 'e';
        memcpy(&tp.msg[0], 
               &seqNum,
               sizeof(unsigned int));
        if ( (size = send(myTcpSock,
                          &tp,
                          sizeof(tp),
                          0) < 0 ) ) {
           cerr << "send (mytcp-sock): "
                << strerror(errno) << endl;
        }
        delete deltaList.front();
        deltaList.pop_front();
        if ( !deltaList.empty() )
          deltaList.front()->activate();
      } 
    }
}

//
// the init_sockets function will set up global varialbes 
// for socket communications.
// 
// TCPD process.
//
void init_sockets() {
  int status;
  struct addrinfo hints;
  struct addrinfo *localCommInfo, *myTcpCommInfo;

  memset(buf, 0, MAX_PACKET_SIZE);
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  // Setup communications
  if ( (status = getaddrinfo(NULL,
                             sLocalPort,
                             &hints,
                             &localCommInfo)) ){
    cerr << "getaddrinfo (local-comm): "
         << gai_strerror(status) << endl;
    exit(1);
  }

  if ( (status = getaddrinfo(NULL,
                             sMytcpPort,
                             &hints,
                             &myTcpCommInfo)) ){
    cerr << "getaddrinfo (local-comm): "
         << gai_strerror(status) << endl;
    exit(1);
  }
  
  // Open Socket for communication
  localComm = socket(localCommInfo->ai_family,
                     localCommInfo->ai_socktype,
                     localCommInfo->ai_protocol);

  myTcpSock = socket(myTcpCommInfo->ai_family,
                     myTcpCommInfo->ai_socktype,
                     myTcpCommInfo->ai_protocol);

  // bind to local port
  if ( (status = bind(localComm,
                      localCommInfo->ai_addr,
                      localCommInfo->ai_addrlen)) ){
    cerr << "bind (local-comm): "
         << strerror(errno) << endl;
    exit(2);
  }

  // connect
  if( (status = connect(myTcpSock, 
                        myTcpCommInfo->ai_addr,
                        myTcpCommInfo->ai_addrlen)) ) {
    cerr << "connect (local-comm): "
         << strerror(errno) << endl;
    exit(3);
  }

  // free resources
  freeaddrinfo(localCommInfo);
}

//
// The build_select helper initalizes the sets
// used with the select function and timeouts
//
// return:
//   the max file descriptor + 1.
int build_select() {
  // Clear out set.
  FD_ZERO(&readfds);

  // Add socket descriptors to sets
  FD_SET(localComm, &readfds);

  // Set the initial timeout value
  tv.tv_sec = 0;
  tv.tv_usec = 50000;
  return (localComm + 1);
}

// pretty print the list to the terminal.
void print_delta_list() {
  list<Timer*>::iterator it;
  int i = 0;
  for (it = deltaList.begin(); it != deltaList.end(); ++it) {
    cout << ++i << ": " << (*it)->pretty_print() << endl;
  }
  cout << endl;
}

// add_timer is to process the add timer command 
// recieved from the TCPDc process.
void add_timer(timer_packet &tp) {
  float timerValue;
  unsigned int seqNum;
  memcpy(&timerValue, &tp.msg[4], sizeof(float));
  memcpy(&seqNum, &tp.msg[0], sizeof(unsigned int));
  Timer *pNewTimer = new Timer(timerValue, seqNum);
 
  // start at the front and traverse the list to find 
  // a spot to insert the timer and accumlate delta times
  list<Timer*>::iterator it;
  for ( it = deltaList.begin(); it != deltaList.end(); ++it) {
    if ( *pNewTimer < *(*it)) {
      break;
    } else {
      pNewTimer->set_timer(pNewTimer->time_till_expire() - (*it)->time_till_expire());
    }
  } 

  // Update the timer's ttl to reflect the delta time.
  // and inserte it into the list if it is replacing the
  // head of the list deactivate that timer first.
  if ( it == deltaList.begin() && deltaList.begin() != deltaList.end()) {
    deltaList.front()->deactivate();
  }
  double insVal = pNewTimer->time_till_expire(); 
  deltaList.insert(it, pNewTimer);

  // Update the rest of the list after the inserted timer
  if (it != deltaList.end()) {
    (*it)->set_timer((*it)->time_till_expire() - insVal); 
  }
  
  // Activate the first timer
  deltaList.front()->activate();
  print_delta_list();
}

// cancel_timer is to process the cancel timer 
// command recievec from TCPDc process.
void cancel_timer(timer_packet &tp) {
  unsigned int seqNum;
  memcpy(&seqNum, &tp.msg[0], sizeof(unsigned int));
  list<Timer*>::iterator it = deltaList.begin();
  while ( it != deltaList.end() ) {
    if ( (*it)->get_sequence_number() == seqNum ) {
      float delTimeVal = (*it)->time_till_expire();
      it = deltaList.erase(it);
      // update next timer if it is not the end
      if (it != deltaList.end() )
        (*it)->set_timer((*it)->time_till_expire() + delTimeVal);
    } else {
      ++it;
    }
  }
  if (!deltaList.empty()) {
    deltaList.front()->activate();
  }
  print_delta_list();
}

void process_requests() {
  ssize_t size;
  if ( (size = read(localComm,
                    buf, 
                    MAX_PACKET_SIZE)) < 0 ){
  cerr << "recv (local-comm) failure" << endl;
  }

  // of the message
  if ( size >= (int) sizeof(timer_packet) ) {
    // copy data to tp structure
    timer_packet tp;
    memcpy(&tp, buf, sizeof(timer_packet));

    // check command
    switch(tp.command) {
      case 'a':
        add_timer(tp);
        break;
      case 'c':
        cancel_timer(tp);
        break;
      default:
        // Ignore packet.
        break;
    }
    
  } else {
    // Ignore packet because it is not valid.
  }
}

int main (int argc, char** argv) {
  if ( argc < 3 ) {
    print_usage(argv[0]);
    exit(-1);
  }
  sLocalPort = argv[1];
  sMytcpPort = argv[2];

  init_sockets();
  // Start main processing loop decrementing timers and
  // waiting on UDP command requests.
  cout << "Waiting On Port (" << sLocalPort << ")...\n";
  int hits, maxfd;
  while ( 1 ) {
    maxfd = build_select();
    // check for requests.
    if ( (hits = select(maxfd, &readfds, NULL, NULL, &tv)) < 0 ) {
      cerr << "select: " << strerror(errno) << endl;
    } else {
      // If request present slelect it and process.
      if ( hits > 0 ) {
        process_requests();
      }
    }
    
    // decrement timer and store current
    // time before going to sleep
    decrement_timer();
  }
  close(localComm);
  return 0;
}
