//
// FILE: ftps
// AUTHOR: Mohammed Shareef
// DESCRIPTION:
//  ftps is a simple server ftp application built on top of the MyTCP service.
//

/* Server for accepting an Internet stream connection on port 1040 */
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>


#include "socket.h"

using namespace std;
using namespace mytcp;

/* server program called with no argument */
int main(int argc, char *argv[])
{
  int sock;                     /* initial socket descriptor */
  int msgsock;                  /* accepted socket descriptor,
                                 * each client connection has a
                                 * unique socket descriptor*/ 
  struct sockaddr_in sin_addr; /* structure for socket name setup */
  const int maxBufferSize = 1000;
  int fileNameSize = 20;
  char buf[maxBufferSize];               /* buffer for holding read data */

  if(argc < 2) {
    printf("usage: ftps local_port\n");
    exit(1);
  }

  int port = atoi(argv[1]);
  if (port < 5000 || port > 65535) {
    perror("error using port number outside [5000, 65535]");
    exit(1);
  }
 
  /*initialize socket connection in unix domain*/
  if((sock = SOCKET(AF_INET, SOCK_STREAM, 0)) < 0){
    perror("error openting datagram socket");
    exit(1);
  }
  
  int adderlen = (int)sizeof(struct sockaddr_in);

  /* bind socket name to socket */
  if(BIND(sock, (struct sockaddr *)&sin_addr, adderlen) < 0) {
    perror("error binding stream socket");
    close(sock);
    exit(1);
  }
  
  /* listen for socket connection and set max opened socket connetions to 5 */
  listen(sock, 5);
  printf("TCP server waiting for remote connection from clients ...\n");
  /* accept a (1) connection in socket msgsocket */ 
  msgsock = ACCEPT(sock, (struct sockaddr *)&sin_addr, adderlen);
  if(msgsock == -1) { 
    perror("error connecting stream socket");
    close(sock);
    exit(1);
  }
  printf("TCP server connected to remote client ...\n");
  
  /* put all zeros in buffer (clear) */
  bzero(buf,maxBufferSize);
  
  printf("TCP server waiting for data from client ...\n");
  
  // start reading in from buffer
  int readBytes= RECV(msgsock, buf, maxBufferSize, 0);
  if(readBytes < 0) {
    perror("error reading on stream socket");
    close(sock);
    exit(1);
  }
  // first buffer will contain: number of bytes to follow, name of file, and file data
  // number of bytes to follow: 4 bytes
  cout<<"Bytes read (ftps): "<< readBytes << endl;
  int bytes_to_follow;
  char * btof[4];
  memcpy(btof, &buf, 4);
  bytes_to_follow = atoi(buf); /* convert int string to int */
  cout<<"Bytes to follow: " << bytes_to_follow<< endl;
  // name of file: 20 bytes
  char filename [fileNameSize];
  memcpy(filename, &buf[4], fileNameSize);
  FILE *received_file = fopen(buf,"w"); /* open new file for writing */
  cout<<"File name: " << filename<< endl;
  // the rest is file data
  readBytes = readBytes - 24;
  if (readBytes >= bytes_to_follow) {
    fwrite(&buf[24], 1, bytes_to_follow, received_file); /* write bytes */
  }
  else {
    fwrite(&buf[24], 1, readBytes, received_file); /* write bytes */
  }
  // subtract this file data from bytes to follow
  bytes_to_follow = bytes_to_follow - readBytes;
  // keep reading from socket until bytes to follow is 0
  bzero(buf,maxBufferSize); /* clear buffer */
  while(bytes_to_follow > 0) {
    readBytes = RECV(msgsock, buf, maxBufferSize, 0);
    if(readBytes < 0) {
      perror("error reading on stream socket");
      close(sock);
      exit(1);
    }
    fwrite(buf, 1, readBytes, received_file); /* write bytes */
    bytes_to_follow = bytes_to_follow - readBytes; // subtract this file data from bytes to follow
    cout<<"bytes to follow" << bytes_to_follow<< endl;
    bzero(buf,maxBufferSize); /* clear buffer */
  }
  
  /* close file */
  fclose(received_file);
  printf("Server closed file.\n");
 
  /* close all connections and remove socket file */
  close(msgsock);
  close(sock);
  printf("Server closed socket.\n");
}
