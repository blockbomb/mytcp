//
// FILE: TCPD_c.cpp
// AUTHOR: Brian Block
//
// MyTCPc runs on the client system waiting to to recieve
// a request from a local process and sent it out to the
// server.

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <math.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

#include "types.h"
#include "WrapAroundBuffer.h"
#include "CRC.h"
#include "constants.h"

using namespace std;
using namespace mytcp::util;

// Global Variables
//

char *sTrollHost;
char *sTrollPort;
int timerListenSock, ftpcListenSock, trollListenSock;
int timerSendSock, ftpcSendSock, trollSendSock;
int nextSeqNum;
bool closeConnection = false;

float RTO = 3.0;
float estimatedRTT = 0;
float devRTT = 0;

char buf[MAX_PACKET_SIZE];

fd_set readfds;
struct timeval tv;
struct timeval rtt_time;

WrapAroundBuffer sendBuff;
int sendBuffSize = sendBuff.size();

ofstream myFile;

// window contains the packet waiting to be ack'ed
// and a boolean determining if it is acked
vector<pair <troll_packet, bool> > window;
map< int, struct timeval > startTimes;

struct sockaddr_in serverAddress;

//
//  prints out the usage of the application
//
void print_usage(char* ProgName)
{
  cout << ProgName << " <troll-host> <troll-port>\n"
       << "   <troll-host>  - the name of the remote host running troll.\n"
       << "   <troll-port>  - the port the troll process is awaiting requests\n"
       << "                   on.\n";
}

//
// init initalizes the global variables and connections required
// for the various communications with the MyTCPc process.
//
void init()
{
  int status;
  struct addrinfo hints;
  struct addrinfo *timerListenInfo, *trollListenInfo, *ftpcListenInfo;
  struct addrinfo *timerSendInfo, *ftpcSendInfo, *trollSendInfo;
  struct addrinfo *ftpsInfo;

  memset ( buf, 0, MAX_PACKET_SIZE );
  nextSeqNum = STARTING_SEQ_NUM;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;
  // Setup local, remote, and troll communications
  if ( (status = getaddrinfo(NULL, MY_TCP_TIMER_LISTEN_PORT,
                             &hints,
                             &timerListenInfo)) ) {
    cerr << "getaddrinfo (local-comm): "
         << gai_strerror(status) << endl;
    exit(1);
  }
  if ( (status = getaddrinfo(NULL, TIMER_PORT,
                             &hints,
                             &timerSendInfo)) ) {
    cerr << "getaddrinfo (timer-send): "
         << gai_strerror(status) << endl;
    exit(1);
  }


  if ( (status = getaddrinfo(NULL, MY_TCP_FTP_LISTEN_PORT,
                             &hints,
                             &ftpcListenInfo)) ) {
    cerr << "getaddrinfo (ftpc-listen): "
         << gai_strerror(status) << endl;
    exit(1); }
  if ( (status = getaddrinfo(NULL, FTPC_PORT,
                             &hints,
                             &ftpcSendInfo)) ) {
    cerr << "getaddrinfo (ftpc-send): "
         << gai_strerror(status) << endl;
    exit(1);
  }

  if ( (status = getaddrinfo(NULL, MY_TCP_TROLL_LISTEN_PORT,
                             &hints,
                             &trollListenInfo)) ) {
    cerr << "getaddrinfo (troll-listen): "
         << gai_strerror(status) << endl;
    exit(1);
  }
  if ( (status = getaddrinfo(sTrollHost,
                             sTrollPort,
                             &hints,
                             &trollSendInfo)) ) {
    cerr << "getaddrinfo (troll-send): "
         << gai_strerror(status) << endl;
  }

  if ( (status = getaddrinfo(DEFAULT_FTPS_HOST,
                             MY_TCP_TROLL_LISTEN_PORT,
                             &hints,
                             &ftpsInfo)) ) {
    cerr << "getaddrinfo (ftps-send)"
         << gai_strerror(status) << endl;
  }

  // open sockets for communication.
  timerListenSock = socket(timerListenInfo->ai_family,
                           timerListenInfo->ai_socktype,
                           timerListenInfo->ai_protocol);
  timerSendSock = socket (timerSendInfo->ai_family,
                          timerSendInfo->ai_socktype,
                          timerSendInfo->ai_protocol);

  ftpcListenSock = socket(ftpcListenInfo->ai_family,
                          ftpcListenInfo->ai_socktype,
                          ftpcListenInfo->ai_protocol);
  ftpcSendSock = socket(ftpcSendInfo->ai_family,
                        ftpcSendInfo->ai_socktype,
                        ftpcSendInfo->ai_protocol);

  trollListenSock = socket(trollListenInfo->ai_family,
                           trollListenInfo->ai_socktype,
                           trollListenInfo->ai_protocol);
  trollSendSock = socket(trollSendInfo->ai_family,
                         trollSendInfo->ai_socktype,
                         trollSendInfo->ai_protocol);

  // bind listening sockets
  if ( (status = bind(timerListenSock,
                      timerListenInfo->ai_addr,
                      timerListenInfo->ai_addrlen)) ) {
    cerr << "bind (timer-listen): " << strerror(errno) << endl;
    exit(2);
  }

  if ( (status = bind(ftpcListenSock,
                      ftpcListenInfo->ai_addr,
                      ftpcListenInfo->ai_addrlen)) ) {
    cerr << "bind (ftpc-listen): "
         << strerror(errno) << endl;
    exit(2);
  }

  if ( (status = bind(trollListenSock,
                      trollListenInfo->ai_addr,
                      trollListenInfo->ai_addrlen)) ) {
    cerr << "bind (troll-listen): "
         << strerror(errno) << endl;
    exit(2);
  }

  // connect sending sockets
  if ( (status = connect(timerSendSock,
                         timerSendInfo->ai_addr,
                         timerSendInfo->ai_addrlen)) ) {
    cerr << "connect (timer-send): " << strerror(errno)
         << endl;
    exit(3);
  }

  if ( (status = connect(ftpcSendSock,
                         ftpcSendInfo->ai_addr,
                         ftpcSendInfo->ai_addrlen)) ) {
    cerr << "connect (ftpc-send): " << strerror(errno)
         << endl;
    exit(3);
  }

  if ( (status = connect(trollSendSock,
                         trollSendInfo->ai_addr,
                         trollSendInfo->ai_addrlen)) ) {
    cerr << "connect (troll-comm): " << strerror(errno)
         << endl;
    exit(3);
  }

  // Copy address for troll to forward to
  memcpy(&serverAddress, (struct sockaddr_in *) ftpsInfo->ai_addr ,
         sizeof (serverAddress) );

  // Print out local and remote communications for a sanity check.
  char str[INET_ADDRSTRLEN];
  inet_ntop(AF_INET, &((struct sockaddr_in *)
                       timerListenInfo->ai_addr)->sin_addr,
            str, INET_ADDRSTRLEN);
  cout << "Timer Communications \n"
       << "  Listening: "
       << str << ":" << ntohs(((struct sockaddr_in *)
                               timerListenInfo->ai_addr)->sin_port)
       << endl;
  memset (str, 0, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &((struct sockaddr_in *)
                       timerSendInfo->ai_addr)->sin_addr,
            str, INET_ADDRSTRLEN);
  cout << "  Sending: "
       << str << ":" << ntohs(((struct sockaddr_in *)
                               timerSendInfo->ai_addr)->sin_port)
       << endl;

  memset (str, 0, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &((struct sockaddr_in *)
                       ftpcListenInfo->ai_addr)->sin_addr,
            str, INET_ADDRSTRLEN);
  cout << "FTPc Communications \n"
       << "  Listening: "
       << str << ":"
       << ntohs(((struct sockaddr_in *)
                 ftpcListenInfo->ai_addr)->sin_port)
       << endl;
  memset (str, 0, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &((struct sockaddr_in *)
                       ftpcSendInfo->ai_addr)->sin_addr,
            str, INET_ADDRSTRLEN);
  cout << "  Sending: "
       << str << ":"
       << ntohs(((struct sockaddr_in *)
                 ftpcSendInfo->ai_addr)->sin_port)
       << endl;

  memset (str, 0, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &((struct sockaddr_in *)
                       trollListenInfo->ai_addr)->sin_addr,
            str, INET_ADDRSTRLEN);
  cout << "Troll Communications \n"
       << "  Listening: "
       << str << ":"
       << ntohs(((struct sockaddr_in *)
                 trollListenInfo->ai_addr)->sin_port)
       << endl;

  memset (str, 0, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &((struct sockaddr_in *)
                       trollSendInfo->ai_addr)->sin_addr,
            str, INET_ADDRSTRLEN);
  cout << "  Send: "
       << str << ":"
       << ntohs(((struct sockaddr_in *)
                 trollSendInfo->ai_addr)->sin_port) << endl;

  freeaddrinfo(timerListenInfo);
  freeaddrinfo(ftpcListenInfo);
  freeaddrinfo(trollListenInfo);
  freeaddrinfo(trollSendInfo);
  freeaddrinfo(ftpsInfo);
}

//
// build select is used to build the set of file descriptors
// that the select command is monitoring for each iteration
// of the processing loop.
//
// Return:
//   int - the max file descriptor in the set + 1
//
int build_select()
{
  FD_ZERO(&readfds);

  FD_SET(timerListenSock, &readfds);
  FD_SET(trollListenSock, &readfds);
  FD_SET(ftpcListenSock, &readfds);


  tv.tv_sec = 0;
  tv.tv_usec = 500000;
  return (
           ( timerListenSock > trollListenSock ?
             (timerListenSock > ftpcListenSock ? timerListenSock : ftpcListenSock) :
               (trollListenSock > ftpcListenSock ? trollListenSock : ftpcListenSock)
             ) + 1);
}

//
// Pretty print the sending buffer this provides a pleasent
// depiction of the send buffer within the terminal running MyTCPc
//
void print_sendBuff()
{
  cout << "[";
  float percent = (float)sendBuff.used()/(float)sendBuffSize;
  int numOfCharacters = percent * 14;
  int i;
  for ( i = 0; i < numOfCharacters; i++ ) {
    cout << "+";
  }

  for ( i = 0; i < 14 - numOfCharacters; i++ ) {
    cout << "_";
  }
  cout << "]";
}

//
// Pretty print the sending window privides a pleasent depiction
// of the window withing the terminal running MyTCPc.
// the seq numbers are displayed within [ ... ] seperated by
// commas.  for packets that have been acked their sequence numbers
// are surrounded by '()'
//
// eg window.
// [ 1, 2, (3), 4, (5)]
//
void print_window()
{
  vector< pair<troll_packet, bool> >::iterator it;
  int seqNum;
  char space[120];
  memset(space, 0, 120);
  memset(space, 0x20, 119);
  cout << "\r" << space;
  cout << "\r[ ";
  for ( it = window.begin(); it < window.end(); it++ ) {
    seqNum = ((mytcp_packet*)(it->first.msg))->header.seqNum;
    if ( it == window.end() -1 ) {
      if (it->second) {
        cout << "(" << seqNum << ")";
      } else {
        cout << seqNum ;
      }
    } else {
      if (it->second) {
        cout << "(" << seqNum << ")|";
      } else {
        cout << seqNum << "|";
      }
    }
  }
  cout << " ]";
  cout.flush();
}

//
// Sent message to add timer to the deltal list
//
void add_timer(float time, unsigned int sn)
{
  ssize_t size;
  timer_packet tp;
  memset(&tp, 0, sizeof(tp));
  tp.command = 'a';
  memcpy(&tp.msg[4], &time, sizeof(float));
  memcpy(&tp.msg[0], &sn, sizeof(unsigned int));

  if ( (size = send ( timerSendSock, &tp, sizeof(tp), 0)) < 0 ) {
    cerr << "send (timer-comm): " <<  strerror(errno);
  }
}

//
// Send message to cancel a timers associated
// with the sequence number from the delta list.
//
void cancel_timer(unsigned int sn)
{
  ssize_t size;
  timer_packet tp;
  memset( &tp, 0, sizeof(tp) );
  tp.command = 'c';
  memcpy(&tp.msg[0], &sn, sizeof(unsigned int));

  if ( (size = send ( timerSendSock, &tp, sizeof(tp), 0)) < 0 ) {
    cerr << "send (timer-comm): " <<  strerror(errno);
  }
}

//
// resend packet is a helper function to resend an un-acknowledged
// packet withing the sending window.
//
void resend_packet(int seqNum)
{
  // remove from startTimes retransmitted packets do not effect RTT
  if ( startTimes.find ( seqNum ) != startTimes.end() ) {
    startTimes.erase ( startTimes.find(seqNum) );
  }
  vector< pair<troll_packet, bool> >::iterator it;
  for ( it = window.begin(); it < window.end(); it++ ) {
    int size = 0;
    if ( ((mytcp_packet*)(it->first.msg))->header.seqNum == seqNum ) {
      if ( (size = send ( trollSendSock,
                          &(it->first),
                          sizeof(troll_packet),
                          0)) < 0 ) {
        cerr << "Retransmission of ( " << seqNum << " ) failed\n";
        add_timer(0, seqNum);
      } else {
        add_timer(RTO, seqNum);
      }
      break;
    }
  }
}

//
// When we recieve an ACK find it in the window
// set the ack boolean to true and slide the window
// if need be.
//
void acknowledge_packet( int seqNum )
{
  vector< pair<troll_packet, bool> >::iterator it;
  for ( it = window.begin(); it < window.end(); it++ ) {
    if ( ((mytcp_packet*)(it->first.msg))->header.seqNum
         == seqNum ) {
      cancel_timer(seqNum);
      it->second = true;

      // calculate the new rtt
      if ( startTimes.find( seqNum ) != startTimes.end()) {
        struct timeval currentTime;
        gettimeofday(&currentTime, NULL);
        double sampleRTT = (currentTime.tv_usec - startTimes.find(seqNum)->second.tv_usec) / 1000000.0;
        estimatedRTT = (1 - ALPHA) * estimatedRTT + ALPHA * sampleRTT;
        devRTT = (1 - BETA) * devRTT + BETA * fabs(sampleRTT - estimatedRTT);
        RTO = estimatedRTT + U * devRTT;
        startTimes.erase ( startTimes.find(seqNum) );
      }
      break;
    }
  }

  // Slide window
  while ( window.size() > 0 && it == window.begin()) {
    if ( window.front().second ) {
      myFile.open("out.jpg", ios::binary | ios::app);
      myFile.write( ((mytcp_packet*)(window.front().first.msg))->msg,
                    window.front().first.msgSize - sizeof(mytcp_header));
      myFile.close();
      window.erase(window.begin());
    } else {
      break;
    }
  }
}

//
// Handler for processing packets recieved on the timer
// channel of communications.
//
void process_timerComm()
{
  // We have a timer packet (supposedly)
  ssize_t size = 0;
  if ( (size = recv ( timerListenSock,
                      buf,
                      MAX_PACKET_SIZE,
                      0)) < 0 ) {
    cerr << "recv (timer-listen): "
         << strerror(errno) << endl;
  }
  timer_packet tp;
  memset(&tp, 0, sizeof(tp));
  memcpy(&tp, buf, size);
  unsigned int seqNum;
  memcpy(&seqNum, &tp.msg[0], sizeof(unsigned int));

  // timeout happened double RTO and resend
  RTO = (RTO * 2) > MAX_RTO ? MAX_RTO : RTO * 2;
  resend_packet( seqNum );
}

//
// Handler for processing packets recieved on the troll
// channel of communications.
//
void process_trollComm()
{
  // we have a troll packet
  ssize_t size = 0;
  if ( (size = recv ( trollListenSock,
                      buf,
                      MAX_PACKET_SIZE,
                      0)) < 0 ) {
    cerr << "recv (troll-listen): "
         << strerror(errno) << endl;
  }

  troll_packet tPkt;
  mytcp_packet pkt;

  memset(&tPkt, 0, sizeof(tPkt));
  memset(&pkt, 0, sizeof(pkt));

  if (size > (int)sizeof(tPkt.header) ) {
    memcpy(&tPkt, buf, size);
    if (tPkt.msgSize >= 0 && tPkt.msgSize <= MAX_TCP_PACKET_SIZE) {
      memcpy(&pkt, tPkt.msg, tPkt.msgSize );
      unsigned short tmpsum = ((mytcp_packet*)tPkt.msg)->header.checksum;
      ((mytcp_packet*)tPkt.msg)->header.checksum = (unsigned short)0;
      if ( tmpsum == crc_generate((unsigned char*)&pkt, tPkt.msgSize , 2)) {
        // garbled drop the packet
        int possibleAckNumber = ((mytcp_packet*)tPkt.msg)->header.ackNum;
        if ( possibleAckNumber >= 0 && possibleAckNumber < MAX_SEQ_NUMB ) {
          resend_packet( possibleAckNumber );
        }
        return;
      }
      
      if ( pkt.header.flags & 0x10 ) {
        // ACK flag set
        acknowledge_packet(pkt.header.ackNum);
      }
    }
  }
}

//
// Handler for processing packets recieved on the ftpc
// channel of communications.
//
void process_ftpcComm()
{
  // we have a ftpc packet
  ssize_t size = 0;
  if ( (size = recv ( ftpcListenSock,
                      buf,
                      MAX_PACKET_SIZE,
                      0)) < 0 ) {
    cerr << "recv (ftpc-listen): "
         << strerror(errno) << endl;
  }

  ftp_packet pkt;
  ftp_packet sendPkt;
  memset(&pkt, 0, sizeof (pkt));
  memset(&sendPkt, 0, sizeof (sendPkt));

  if ( size >= (int)sizeof(pkt.header) ) {
    memcpy (&pkt, buf, size);
    int msgSize(0);
    switch ( pkt.header.command ) {
    case 'd':
      msgSize = size - sizeof(pkt.header);
      sendBuff.write_data(pkt.msg, msgSize );
      break;
    case 'q':
      sendPkt.header.command = 'r';
      sendPkt.header.bytes_available = sendBuff.unused();

      if ( (size = send ( ftpcSendSock,
                          &sendPkt,
                          sizeof(sendPkt.header),
                          0)) < 0) {
        cerr << "\nftp response failed: \n"
             << strerror(errno) << endl;
      }
      break;
    case 'f':
      closeConnection = true;
      break;
    }
  }
}

//
// fill up a packet with up to one MSS of data from the
// send buffer and add to the window.
//
void queue_packet()
{
  mytcp_packet pkt;
  troll_packet tPkt;

  memset(&pkt, 0, sizeof(mytcp_packet));
  memset(&tPkt, 0, sizeof(troll_packet));

  // set up mytcp header information
  pkt.header.seqNum = nextSeqNum++ % MAX_SEQ_NUMB;
  int msgSize = sendBuff.read_data(pkt.msg, MAX_MSG_SIZE);
  int sendingSize = sizeof(mytcp_header) + msgSize;

  // run checksum
  unsigned short crc = crc_generate((unsigned char*)&pkt, sizeof(pkt.header) + msgSize, 2);
  memcpy(&pkt.header.checksum, &crc, sizeof(unsigned short));

  // set up troll forwarding header information
  tPkt.header.sin_family = htons(serverAddress.sin_family);
  tPkt.header.sin_addr = serverAddress.sin_addr;
  tPkt.header.sin_port = serverAddress.sin_port;
  tPkt.msgSize = sendingSize;

  // copy mytcp_packet to troll wrapper.
  memcpy( tPkt.msg, &pkt, sendingSize);

  int size = 0;
  if ( (size = send ( trollSendSock,
                      &tPkt,
                      sizeof(troll_packet) ,
                      0)) < 0 ) {
    cerr << "Sending (troll-comm): "
         << strerror(errno) << endl;
  }
  // add timestamp for rtt calculations
  struct timeval currentTime;
  gettimeofday(&currentTime, NULL);
  startTimes.insert ( pair <int, struct timeval> (pkt.header.seqNum, currentTime) );

  // start timer and add packet to window
  add_timer(RTO, pkt.header.seqNum);
  window.push_back(pair<troll_packet, bool> ( tPkt, false) );
}

//
// fill the window with as much data to send as possible
//
void queue_packets()
{
  while ( (window.size() < WINDOW_SIZE) &&
          !(sendBuff.used() == 0)) {
    queue_packet();
  }
}

//
// helperfunction to delegate responsibilities.
//
void process_requests()
{
  if ( FD_ISSET(timerListenSock, &readfds) ) {
    process_timerComm();
  }

  if ( FD_ISSET(trollListenSock, &readfds) ) {
    process_trollComm();
  }

  if ( FD_ISSET(ftpcListenSock, &readfds) ) {
    process_ftpcComm();
  }
}

//
// close_connection starts the fin loop
// send fin packet wait for fin ack. 
// then wait for fin, send fin ack. 
//
void close_connection() {
  troll_packet tPkt;
  mytcp_packet pkt;

  memset(&tPkt, 0, sizeof(tPkt));
  memset(&pkt, 0, sizeof(pkt));

  pkt.header.flags = FIN_BIT;
  unsigned short crc = crc_generate((unsigned char*)&pkt, sizeof(pkt.header), 2);
  pkt.header.checksum = crc;

  tPkt.header.sin_family = htons(serverAddress.sin_family);
  tPkt.header.sin_addr = serverAddress.sin_addr;
  tPkt.header.sin_port = serverAddress.sin_port;
  tPkt.msgSize = sizeof(pkt.header);

  memcpy ( tPkt.msg, &pkt, sizeof (pkt.header) );

  cout << "\nShutting Down Connection..." << endl;
  while ( 1 ) {
    if ( send ( trollSendSock, &tPkt, sizeof(tPkt), 0) < 0 ) {
      cerr << "error sending fin packet" << endl;
    }
    memset ( &tPkt, 0, sizeof(tPkt) );
    if ( recv ( trollListenSock, &tPkt, sizeof(tPkt), 0) < 0 ) {
      cerr << "error receiveing fin ack" << endl ;
    } else {
      if ( ((mytcp_packet*)tPkt.msg)->header.flags && (FIN_BIT | ACK_BIT) ) {
        break;
      }
    }
  }

  while ( 1 ) {
    if ( recv ( trollListenSock, &tPkt, sizeof(tPkt), 0 ) < 0 ) {
      cerr << "error receiving fin..." << endl;
    }

    if ( !((mytcp_packet*)tPkt.msg)->header.flags && FIN_BIT ) {
      cerr << "error sending fin ack.." << endl;
    } else {
      cout << "Exiting Connection." << endl;
      memset(&tPkt, 0, sizeof(tPkt));
      memset(&pkt, 0, sizeof(pkt));

      pkt.header.flags = FIN_BIT | ACK_BIT; 
      
      tPkt.header.sin_family = htons(serverAddress.sin_family);
      tPkt.header.sin_addr = serverAddress.sin_addr;
      tPkt.header.sin_port = serverAddress.sin_port;
      tPkt.msgSize = sizeof(pkt.header);
      memcpy(tPkt.msg, &pkt, sizeof(pkt.header));

      send ( trollSendSock, &tPkt, sizeof(tPkt), 0);
      break;
    }
  }
}

//
// Entry point and source of the event loop
//
int main (int argc, char** argv )
{
  if (argc < 3) {
    print_usage(argv[0]);
    return -1;
  }

  sTrollHost = argv[1];
  sTrollPort = argv[2];

  init();
  cout << endl;

  // Start main loop and wait for messages.
  int hits=0, maxfd=0;
  while ( 1 ) {
    maxfd = build_select();
    memset(buf, 0, MAX_PACKET_SIZE);
    // Check for requests
    if ( (hits = select(maxfd, &readfds, NULL, NULL, &tv)) < 0 ) {
      cerr << "Select: " << strerror(errno) << endl;
    } else {
      //If request present select it and process
      if ( hits > 0 ) {
        process_requests();
      }

      if ( window.size() < WINDOW_SIZE && 
           !(sendBuff.used() == 0)) {
        queue_packets();
      }

      if ( window.size() == 0 &&
           sendBuff.used() == 0 && 
           closeConnection ) {
        close_connection();
        break;
      }
      print_window();
      print_sendBuff();
      cout.flush();
    }
  }
  close(timerListenSock);
  close(trollListenSock);
  close(ftpcListenSock);
  return 0;
}
