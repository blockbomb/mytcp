//
// FILE: MyTCPs.cpp
// AUTHOR: Mohammed Shareef 
//
// MyTCPs runs on the server system waiting to recieve
// a request from a local process and sent it out to the
// server.

#include <stdio.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <vector>
#include <queue>
#include <map>
#include <unistd.h>
#include <sys/time.h>
#include <cstring>
#include <math.h>
#include <iostream>
#include <sys/select.h>
#include <errno.h>

#include "types.h"
#include "WrapAroundBuffer.h"
#include "CRC.h"
#include "constants.h"

using namespace std;
using namespace mytcp::util;

/* Global Variables */
int trollSock, ftpsSock; //sockets
int trollnamelen, ftpsnamelen; //address domains
int trollSendSock;
char srv_buf[MAX_PACKET_SIZE];
char* sTrollHost, *sTrollPort;
struct sockaddr_in ftps_addr, troll_addr;
struct sockaddr_in forwardAddress;
struct hostent *gethostbyname();

WrapAroundBuffer sendBuff; //buffer for troll packets
int sendBuffSize = sendBuff.size();
WrapAroundBuffer recvBuff; //buffer for client packets
int recvBuffSize = recvBuff.size();


// window contains the packet waiting to be ack'ed
// and a boolean determining if it is acked
vector <pair <troll_packet, int> > window;
map< int, struct timeval > startTimes;

float RTO = 3.0;
float estimatedRTT = 0;
float devRTT = 0;

char buf[MAX_PACKET_SIZE];
fd_set readfds;

int nextSeqNum;
int lowerAckBound;
struct timeval tv;
struct timeval rtt_time;

/////////////////////////////////////////////////////////////

//creates troll and ftps sockets and binds them
void initialize() {

  nextSeqNum = STARTING_SEQ_NUM;
  lowerAckBound = 0;
  struct addrinfo hints;
  struct addrinfo *ftpcInfo, *trollSendInfo;

  memset(&forwardAddress, 0, sizeof(forwardAddress));
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  
  int status;
  if ( (status = getaddrinfo(DEFAULT_FTPC_HOST,
                             MY_TCP_TROLL_LISTEN_PORT,
                             &hints,
                             &ftpcInfo)) ) {
    cerr << "getaddrinfo (ftpc-info): "
         << gai_strerror(status) << endl;
  }

  if ( (status = getaddrinfo(sTrollHost, 
                             sTrollPort,
                             &hints,
                             &trollSendInfo)) ) {
    cerr << "getaddrinfo (troll-send): "
         << gai_strerror(status) << endl;
  }

  memcpy(&forwardAddress, (struct sockaddr_in*) ftpcInfo->ai_addr , sizeof (forwardAddress) );

  trollSendSock = socket(trollSendInfo->ai_family,
                         trollSendInfo->ai_socktype,
                         trollSendInfo->ai_protocol);

  if ( (status = connect(trollSendSock,
                         trollSendInfo->ai_addr,
                         trollSendInfo->ai_addrlen)) ) {
   cerr << "connect (mytcp-comm): "
        << strerror(errno) << endl; 
   exit ( 1 );
  }

  /*create troll socket*/
  trollSock = socket(AF_INET, SOCK_DGRAM, 0); //Unix address domain used for two processes on same machine
  if(trollSock < 0) {
    perror("opening troll datagram socket");
    exit(1);
  }
  /* create troll address with parameters and bind troll address to socket */
  troll_addr.sin_family = AF_INET;
  troll_addr.sin_port = htons(atoi(MY_TCP_TROLL_LISTEN_PORT)); // assign port number in network byte order
  troll_addr.sin_addr.s_addr = INADDR_ANY; //gets the address of the machine and assigns it to this socket address
  if(bind(trollSock, (struct sockaddr *)&troll_addr, sizeof(troll_addr)) < 0) {
    perror("getting troll socket name");
    exit(2);
  }

  /*create ftps socket for ftps communication*/
  ftpsSock = socket(AF_INET, SOCK_DGRAM, 0);
  if(ftpsSock < 0) {
    perror("opening ftps datagram socket");
    exit(1);
  }
  /* create troll address with parameters and bind troll address to socket */
  ftps_addr.sin_family = AF_INET;
  ftps_addr.sin_port = htons(atoi(FTPS_PORT)); // assign port number in network byte order
  //get the address of the ftps machine and assign it to this socket address
  struct hostent *hp;
  hp = gethostbyname(DEFAULT_FTPS_HOST);
  bcopy((char *)hp->h_addr, (char *)&ftps_addr.sin_addr, hp->h_length);
  
  char str[INET_ADDRSTRLEN];
  memset (str, 0, INET_ADDRSTRLEN);
  inet_ntop(AF_INET, &((struct sockaddr_in *) 
                     trollSendInfo->ai_addr)->sin_addr, 
            str, INET_ADDRSTRLEN);
  cout << "  Send: "
       << str << ":" 
       << ntohs(((struct sockaddr_in *) 
                trollSendInfo->ai_addr)->sin_port) << endl;
}

void unload() {
  close(trollSock);
  close(ftpsSock);
}

void slide_window () {
  vector < pair< troll_packet, int> >::iterator it;
  for ( it = window.begin(); it != window.end(); it++ ) {
    if ( it->second == nextSeqNum ) {
      recvBuff.write_data( it->first.msg, it->first.msgSize );
      window.erase(window.begin());
      nextSeqNum = ( nextSeqNum + 1 ) % MAX_SEQ_NUMB;
      if ( (lowerAckBound + WINDOW_SIZE) % MAX_SEQ_NUMB < nextSeqNum )
        lowerAckBound++;
      break;
    } 
  }
}

// Handler for processing packets recieved on the trollSocket
// channel of communications.
void process_trollComm() {
  // we have a ftpc packet
  ssize_t size = 0;
  
  //get packet from troll socket
  if ( (size = recv ( trollSock, buf, MAX_PACKET_SIZE, 0)) < 0 ) {
    perror("receiving packet on trollSock.");
  }
  //cout <<"Got a packet from troll."<<endl;
  // TODO ftpc packet logic
  // TODO (testing)

  troll_packet tPkt;
  mytcp_packet pkt;
  mytcp_packet ackPkt;

  memset(&tPkt, 0, sizeof(tPkt));
  memset(&pkt, 0, sizeof(pkt));
  memset(&ackPkt, 0, sizeof(ackPkt));

  if (size > (int)sizeof(tPkt.header) ) {
    // check the checksum
    memcpy(&tPkt, buf, size);
    memcpy(&pkt, ((troll_packet*)buf)->msg, size - sizeof(tPkt.header) ); 
    unsigned short chksum = pkt.header.checksum;
    unsigned short tmpsum = 0;
    memcpy(&pkt.header.checksum, &tmpsum, sizeof(unsigned short));
    if ( tPkt.msgSize > 0 && tPkt.msgSize <= MAX_TCP_PACKET_SIZE) {
      tmpsum = crc_generate((unsigned char*)&pkt, tPkt.msgSize , 2);
    } else {
      return;
    }
    if ( chksum != tmpsum) {
      // garbled drop the packet
      cout << "\nGarbled Packet :(\n";
      return;
    }
    memcpy(&pkt.header.checksum, &chksum, sizeof(unsigned short));
    
    //cout << "Recieved packet seq num ( " << pkt.header.seqNum <<  " )" <<endl;
    
    vector< pair<troll_packet, int> >::iterator it;
    ackPkt.header.seqNum = pkt.header.seqNum;
    ackPkt.header.ackNum = pkt.header.seqNum;
    ackPkt.header.flags = 0x10;
    tPkt.msgSize = sizeof(ackPkt.header);
    unsigned short crc = mytcp::util::crc_generate((unsigned char*)&ackPkt, tPkt.msgSize, 2);
    memcpy (&ackPkt.header.checksum, &crc, sizeof (unsigned short) );
    memcpy ( tPkt.msg, &ackPkt, tPkt.msgSize );
    tPkt.header.sin_port = forwardAddress.sin_port;

    // check if it is a duplicate that needs acked
    bool acked = false;
    if ( lowerAckBound < nextSeqNum ) {
      if (pkt.header.seqNum > lowerAckBound && pkt.header.seqNum < nextSeqNum) {
        // ack packet
        if ( (size = send ( trollSendSock,&tPkt, sizeof(tPkt), 0 )) < 0 ) {
          cout << "\nFAILed sending ack.\n";
        } else { 
          acked = true;
        }
        acked = true;
        return;
      }
    }
    else {
      if (pkt.header.seqNum > lowerAckBound || pkt.header.seqNum < nextSeqNum) { 
        // ack packet
        if ( (size = send ( trollSendSock, &tPkt, sizeof(tPkt), 0 )) < 0 ) {
          cout << "\nFAILed sending ack.\n";
        }
        else { 
          acked = true;
        }
        return;
      }
    }

    if ( !acked ) {
      bool addToWindow = true;
      for ( it = window.begin(); it != window.end(); it++ ){ 
        // check if already in receive window
        if ( it->second == pkt.header.seqNum ) {
          addToWindow = false;
          break;
        }
      } 
      if (addToWindow) {
        window.push_back(pair< troll_packet, int> (tPkt, pkt.header.seqNum));
      }
      //  send ack
      if ( (size = send ( trollSendSock, 
                          &tPkt, sizeof(tPkt), 0 )) < 0 ) {
        cout << "\nFAILed sending ack.\n";
      }
      else { 
        acked = true;
      }
    }
  }
}

// fill up a packet with up to one MSS of data from the 
// send buffer and add to the window.
void queue_packet() {
  mytcp_packet pkt;
  troll_packet tPkt;

  memset(&pkt, 0, sizeof(mytcp_packet));
  memset(&tPkt, 0, sizeof(troll_packet));

  // TODO set up mytcp header information
  pkt.header.seqNum = nextSeqNum++ % 60;
  // TODO REMOVE THIS SHIT
  pkt.header.ackNum = pkt.header.seqNum;
  pkt.header.flags = 0x10;
  // END REMOVE
  int msgSize = sendBuff.read_data(pkt.msg, MAX_MSG_SIZE);
  int sendingSize = sizeof(mytcp_header) + msgSize;

  // run checksum
  unsigned short crc = crc_generate((unsigned char*)&pkt, sizeof(pkt.header) + msgSize, 2);
  //
  memcpy(&pkt.header.checksum, &crc, sizeof(unsigned short));
  // set up troll forwarding header information
  tPkt.header.sin_family = htons(ftps_addr.sin_family);
  tPkt.header.sin_addr = ftps_addr.sin_addr;
  tPkt.header.sin_port = ftps_addr.sin_port;
  tPkt.msgSize = sendingSize;

  // copy mytcp_packet to troll wrapper.
  memcpy( tPkt.msg, &pkt, sendingSize);

  int size = 0;
  if ( (size = send ( ftpsSock, &tPkt, sizeof(troll_packet), 0)) < 0 ) {
    perror("Sending (troll-comm, ftpsSock).");
  } 
  // add timestamp for rtt calculations
  struct timeval currentTime;
  gettimeofday(&currentTime, NULL);
  startTimes.insert ( pair <int, struct timeval> (pkt.header.seqNum, currentTime) ); 

  // add packet to window
  window.push_back(pair<troll_packet, bool> ( tPkt, false) );
}

// fill the window with as much data to send as possible
void queue_packets() {
  while ( (window.size() < WINDOW_SIZE) && 
          !(sendBuff.used() == 0)) {
    queue_packet(); 
  }
}

// build select is used to build the set of file descriptors
// that the select command is monitoring for each iteration
// of the processing loop.
int build_select() {
  FD_ZERO(&readfds);

  FD_SET(ftpsSock, &readfds);
  FD_SET(trollSock, &readfds);

  
  tv.tv_sec = 0;
  tv.tv_usec = 50000;
  return ((ftpsSock > trollSock ? ftpsSock : trollSock) + 1);
}

//
// Pretty print the sending buffer this provides a pleasent
// depiction of the send buffer within the terminal running MyTCPc
//
void print_recvBuff()
{
  cout << "[";
  float percent = (float)sendBuff.used()/(float)sendBuffSize;
  int numOfCharacters = percent * 14;
  int i;
  for ( i = 0; i < numOfCharacters; i++ ) {
    cout << "+";
  }

  for ( i = 0; i < 14 - numOfCharacters; i++ ) {
    cout << "_";
  }
  cout << "]";
}

//
// Pretty print the sending window privides a pleasent depiction
// of the window withing the terminal running MyTCPc.
// the seq numbers are displayed within [ ... ] seperated by
// commas.  for packets that have been acked their sequence numbers
// are surrounded by '()'
//
// eg window.
// [ 1, 2, (3), 4, (5)]
//
void print_window()
{
   vector< pair<troll_packet, int> >::iterator it;
  int seqNum;
  char space[120];
  memset(space, 0, 120);
  memset(space, 0x20, 119);
  cout << "\r" << space;
  cout << "\r[ ";
  for ( it = window.begin(); it < window.end(); it++ ) {
    seqNum = (it->second);
    if ( it == window.end() -1 ) {
      cout << seqNum ;
    } else {
      cout << seqNum << "|";
    }
  } 
  cout << " ]";
  cout.flush();
}

void sendPacketsToFtps() {
  ftp_packet pkt;
  memset (&pkt, 0, sizeof(pkt));
 
  pkt.header.command = 'd';
  int size = recvBuff.read_data(pkt.msg, MAX_MSG_SIZE);

  socklen_t len = sizeof(ftps_addr);
  cout << "Attempting to send packets to ftps.\n";
  if ( (size = sendto ( ftpsSock, &pkt, sizeof(pkt.header) + size, 0 ,(struct sockaddr *)&ftps_addr, len )) < 0 ){
    cerr << "\nERROR sending to ftps...\n";
  }
}

int main(int argc, char** argv )
{

  if ( argc < 3 ) {
    cout << "PROGRAM <troll-host> <troll-port>\n";
    exit(1);
  }
  sTrollHost = argv[1];
  sTrollPort = argv[2];

  initialize();

  cout << endl;

  // Start main loop and wait for messages.
  int hits=0, maxfd=0;
  while ( 1 ) {
    maxfd = build_select();
    memset(buf, 0, MAX_PACKET_SIZE);
    // Check for requests
    if ( (hits = select(maxfd, &readfds, NULL, NULL, &tv)) < 0 ) {
      perror("Select function.");
    }
    else {
      //If request present select it and process
      if ( hits > 0 ) {
        //process_requests();
        process_trollComm();
      }

      if ( window.size() < WINDOW_SIZE && !(sendBuff.used() == 0)) {
        queue_packets();
      }
      if ( recvBuff.used() > 0 ) {
        sendPacketsToFtps();
      }
      if ( recvBuff.unused() > 0 && window.size() > 0 ) {
        slide_window();    
      }

      print_window();
      print_recvBuff();
      cout.flush();
    }
  }


  unload();

  exit(0);
}
