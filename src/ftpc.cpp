//
//  FILE: ftpc.cpp
//  AUTHOR: Brian Block
//  DESCRIPTION:
//    ftpc is a simple client client applicaiton built on top of the 
//    MyTCP service.
//

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <errno.h>

#include "constants.h"
#include "types.h"
#include "socket.h"

using namespace std;
using namespace mytcp;

// Global Variables
char* sFileName;
int lSock;

//
// print program usage
void print_usage(char* ProgName) {
  cout << ProgName << " <file-name>\n"
       << "  <file-name>       - the name of the file to send\n";
}

//
// Progress bar is a simple ascii graphic that displays 
// percent done and a spinning wheel.
//
void progressBar(int percent, char* filename) {
  static int x = 0;
  if ( percent < 100 ) {
    string slash[4];
    slash[0] = "\\";
    slash[1] = "-";
    slash[2] = "/";
    slash[3] = "|";
    cout << "\r";
    cout << slash[x] << " " << percent << "% (" << filename << ")";
    x = (x + 1) % 4;
  } else {
    cout << "\r";
    cout << "[DONE] (" << filename << ")                 ";
  }
  cout.flush();
}

// 
// initalize global variables and communication channels.
//
void init() {
  int status;
  struct addrinfo hints;
  struct addrinfo *localInfo;

  memset ( &hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  // Set up local comms
  if ( (status = getaddrinfo(NULL,
                             FTPC_PORT,
                             &hints,
                             &localInfo)) ) {
    cerr << "getaddrinfo (local-comm): "
         << gai_strerror(status) << endl;
    exit(1);
  }

  // Open Sockets
  lSock = SOCKET(localInfo->ai_family,
                 localInfo->ai_socktype,
                 localInfo->ai_protocol);
}

// 
//  Entry point and source of event loop.
//
int main ( int argc, char** argv ) {
  if ( argc < 2 ) {
    print_usage(argv[0]);
    return -1;
  }

  sFileName = argv[1];
  init();
  
  // Open file and begin sending over network.
  ifstream myFile ( sFileName, 
                    ios::binary | ios::in | ios::ate );
  if ( myFile.is_open() ) {
    // read in file to memeory and close
    int fileSize = myFile.tellg();
    char *memblock = new char[fileSize];
    memset(memblock, 0, fileSize);
    myFile.seekg(0, ios::beg);
    myFile.read(memblock, fileSize);
    myFile.close();

    // Start sending file in chunks of MAX_MSG_SIZE or less
    int sendSize=0, bytesSent=0;
    ssize_t size;

    char buf[MAX_MSG_SIZE];
    memset(buf, 0, MAX_MSG_SIZE);
    memcpy(&buf[0], &fileSize, sizeof(int) );
    memcpy(&buf[4], sFileName, strlen(sFileName)); 
    sendSize = (fileSize - bytesSent >= MAX_MSG_SIZE - 24) ? (MAX_MSG_SIZE - 24) : (fileSize - bytesSent);
    memcpy(&buf[24], &memblock[bytesSent], sendSize );
    bytesSent += sendSize; // just file data
    sendSize += 24;  // with header
    if ( (size = SEND (lSock,
                       buf,
                       sendSize,
                       0)) < 0 ) {
      cerr << "SEND error (local-comm): " << strerror(errno) << endl;
      exit(3);
    }

    int i = 0 ;
    while ( bytesSent < fileSize ) {
      if ( (size = SEND (lSock,
            &memblock[bytesSent],
            fileSize - bytesSent,
            0)) < 0 ) {
        cerr << "SEND error (local-comm): " << strerror(errno) << endl;
      }
      bytesSent += size;
      if ( !((++i) % 5) && fileSize > 0) {
        progressBar((float)bytesSent/(float)fileSize *100, sFileName);
      }
      usleep(50000);
    } 
    progressBar(100, sFileName);
    delete[] memblock;
  } else {
    cerr << "opening file (" << sFileName << ") failed...\n";
  }
  CLOSE(lSock);
  return 0;
}
