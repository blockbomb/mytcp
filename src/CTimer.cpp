/*
 * File: CTimer.cpp
 * Author: Brian Block
 *
 * CTimer is the concrete implementation of the Timer.h
 * it encapsulates being a stop watch.
 */

#include "Timer.h"
#include <iostream>
#include <string>
#include <sstream>
#include <ctime>
#include <sys/time.h>

using namespace std;
using namespace mytcp::util;

// please refer to Timer.h for documentation
Timer::Timer(double ttl, unsigned int sn) {
  timeLeft = ttl;
  seqNum = sn;
}

Timer::~Timer() {}

// please refer to Timer.h for documentation
bool Timer::tick() {
  if ( active ) { 
    timeval currentTime; 
    gettimeofday(&currentTime, NULL);
    double timePassed = (currentTime.tv_usec - timeStamp.tv_usec) / 1000000.0;
    timePassed += (currentTime.tv_sec - timeStamp.tv_sec);
    timeLeft -= timePassed;
    gettimeofday(&timeStamp, NULL);
  }
  return (timeLeft < 0) ? true : false;
}

// please refer to Timer.h for documentation
double Timer::set_timer(double ttl) {
  timeLeft = ttl;
  return timeLeft;
}

// please refer to Timer.h for documentation
bool Timer::activate() {
  gettimeofday(&timeStamp, NULL);
  active = true;
  return true;
}

// please refer to Timer.h for documentation
bool Timer::deactivate() {
  active = false;
  return true;
}

// please refer to Timer.h for documentation
double Timer::time_till_expire() const {
  return timeLeft; 
}

// please refer to Timer.h for documentation
const char* Timer::pretty_print() {
  stringstream ss("");
  ss << "( " << seqNum << ", " << timeLeft << " )"; 
  return ss.str().c_str();
}

// please refer to Timer.h for documentation
unsigned int Timer::get_sequence_number() {
  return seqNum;
}

// please refer to Timer.h for documentation
bool Timer::operator< (const Timer &rhs) const {
  return this->timeLeft < rhs.timeLeft;
}

// please refer to Timer.h for documentation
bool Timer::operator> (const Timer &rhs) const {
  return this->timeLeft > rhs.timeLeft;
}
