//
// File: socket.cpp
// Author: Mohammed Shareef
//
// A concrete implementation of the socket interface.
//
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <cstring>
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include <stdio.h>

#include "socket.h"
#include "constants.h"
#include "types.h"

using namespace std;

struct hostent *gethostbyname();

namespace mytcp{

int extsockfd; //file descriptor for external connection
int daemonSock; //socket for receiving messages from daemon
int ftpcListen;
bool ftpcBound = false;

struct addrinfo *daemonInfo;
struct addrinfo *ftpcInfo;
struct sockaddr_in daemon_addr;


  int SOCKET(int domain, int type, int protocol) {
    //TODO fill in SOCKET implementation
    struct addrinfo hints; //template for the preferred options for socket
    
    memset (&hints, 0, sizeof(hints));
    memset(&daemon_addr, 0, sizeof(daemon_addr));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags = AI_PASSIVE;

    int status = 0;
    if ( (status = getaddrinfo ( NULL, MY_TCP_FTP_LISTEN_PORT,
                  &hints,
                  &daemonInfo)) ) {
      cerr << "SOCKET daemon ( " << gai_strerror(status);
    } 

    if ( (status = getaddrinfo (NULL,
                                FTPC_PORT,
                                &hints,
                                &ftpcInfo)) ) {
      cerr << "SOCKET ftpc ( gai_strerror(status)" << gai_strerror(status);
    }

    ftpcListen = socket(ftpcInfo->ai_family,
                      ftpcInfo->ai_socktype,
                      ftpcInfo->ai_protocol);
    if ( !ftpcBound ) {
      if ( (status = bind (ftpcListen,
                           ftpcInfo->ai_addr,
                           ftpcInfo->ai_addrlen)) ) {
        cerr << "SOCKET ftpc bind ";
      } else {
        ftpcBound = true;
      }
    }
    
    daemonSock = socket(domain, SOCK_DGRAM, protocol); //create the socket for MyTCPs

    return daemonSock;
  }

  int BIND(int sockfd, const struct sockaddr *my_addr, socklen_t adderlen) {
    //create the connection to MyTCPs
    daemon_addr.sin_family = AF_INET;
    daemon_addr.sin_port = htons(atoi(FTPS_PORT));
    struct hostent *hp;
    hp = gethostbyname(DEFAULT_FTPS_HOST);
    bcopy((char *)hp->h_addr, (char *)&daemon_addr.sin_addr, hp->h_length);
    
    //bind sockfd to the MyTCPs connection
    my_addr = (struct sockaddr *)&daemon_addr;
    adderlen = sizeof(daemon_addr);
    int success_sockfd = bind(sockfd, my_addr, sizeof(daemon_addr));
    
    return success_sockfd;
  }


  int ACCEPT(int sockfd, struct sockaddr *addr, socklen_t adderlen) {
    //Because this is UDP, the accept function does not actually exist.
    //The same socket is open for all communication.
    cout << "Attempting to accept connection.\n";
    return sockfd;
  }

  int CONNECT(int sockfd, struct sockaddr *addr, 
              socklen_t adderlen){
    // returns 0, because there is no TCP handshake occurring
    return 0;
  }


  int SEND(int sockfd, const void *buf, size_t len,
           int flags) {
    //TODO fill in the SEND implementation
    
    int bytesToSend = len;
    int bytesSent = 0;
    int freeBufferSpace = 0;
    int msgSize = 0;
    ftp_packet pkt, rPkt;
    memset (&pkt, 0, sizeof(pkt));
    // Request bytes avaliable
    pkt.header.command = 'q';
    msgSize = sendto(sockfd, &pkt,  sizeof(pkt.header), 0,
              daemonInfo->ai_addr,  daemonInfo->ai_addrlen ) - sizeof(pkt.header);
    recv ( ftpcListen, &rPkt, sizeof(rPkt), 0 );
    freeBufferSpace = rPkt.header.bytes_available;
    if (bytesToSend > freeBufferSpace ) {
      bytesToSend = freeBufferSpace;
    }
    pkt.header.command = 'd';
    while (bytesToSend > 0) {
      if ( bytesToSend > MAX_MSG_SIZE ) {
        msgSize = MAX_MSG_SIZE; 
      } else {
        msgSize = bytesToSend;
      } 
      memcpy ( pkt.msg, &((char*)buf)[bytesSent], msgSize );
      msgSize = sendto(sockfd, &pkt,  sizeof(pkt.header) + msgSize, flags,
             daemonInfo->ai_addr,  daemonInfo->ai_addrlen ) - sizeof(pkt.header);
      if (msgSize > 0) {
        bytesToSend -= msgSize;
        bytesSent += msgSize;
      } else {
        // error sending
      }
      usleep(5000);
    }
    
    return bytesSent;
  }

  int RECV(int s, void *buf, size_t len, int flags) {
    //TODO fill in the RECV implementation
    int readBytes;
    char recvBuf[sizeof(ftp_packet)]; //create buffer
    bzero(recvBuf,sizeof(ftp_packet));
    ftp_packet fpkt;
    memset(&fpkt, 0, sizeof(fpkt));
    
    //1. receive bytes from daemon thru extsockfd
    readBytes = recv(s, recvBuf,  sizeof(fpkt), flags);
    cout<<"Bytes read (socket.cpp): "<< readBytes << endl;
    puts(recvBuf);
    
    //2. remove header
    memcpy(&fpkt.header.command, &recvBuf[0], 1); //read in 1 byte
    memcpy(&fpkt.header.bytes_available, &recvBuf[1], 4); //read in 4 bytes
    int numMsgBytes = readBytes - sizeof(fpkt.header);
    memcpy(&fpkt.msg, &recvBuf[5], numMsgBytes); //read in the rest
    //3. load bytes into buf
    memcpy(buf, fpkt.msg, sizeof(fpkt.msg));
    
    //4. return bytes loaded, else return -1
    return numMsgBytes;
  }

  int CLOSE(int sockfd) {
    //TODO fill in the CLOSE implementation

    ftp_packet pkt;
    memset(&pkt, 0, sizeof(ftp_packet) );

    pkt.header.command = 'f';
    sendto ( sockfd, &pkt, sizeof(pkt.header), 0, 
             daemonInfo->ai_addr, daemonInfo->ai_addrlen);
    close(extsockfd);
    close(daemonSock);
    return -1;
  }
}
