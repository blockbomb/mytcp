//
// File: WrapAroundBuffer.cpp
// Author: Brian Block
//
// The Wrap around buffer is used to store data abstracting 
// memory management from the user.  It is a FIFO datastructure
// where the first byte written will be the first byte returned.
//
#include "WrapAroundBuffer.h"
#include <string.h>
using namespace mytcp::util;

//
// Constructor initalized the buffer to a predetermined
// value as well as a starting value for the data pointer.
WrapAroundBuffer::WrapAroundBuffer() :
  dataStart(0),
  count(0)
  {
    memset(&buffer[0], 0, WRAP_AROUND_BUFFER_SIZE);
  }
WrapAroundBuffer::~WrapAroundBuffer() {}

//
// write_data will attempt to write len number of bytes
// of data contained in buf, and return the number of data
// bytes written.
//
// Parameters:
//   buf - pointer to a byte buffer.
//   len - the number of bytes from buf to store in the WAB
//
// Return:
//   int - the number of successful bytes written to the buffer. 
//         if the buffer is fairly full the return value may be
//         less then len.
//
int WrapAroundBuffer::write_data(char const* buf, int len) {
  int ret = -1;
  int unused = this->unused();
  int endIndex = (dataStart + count) % WRAP_AROUND_BUFFER_SIZE;
  int dataSizeTillEnd = WRAP_AROUND_BUFFER_SIZE - endIndex;
  if ( len > unused ) {
    // write unusued ammount
    if ( endIndex + unused > WRAP_AROUND_BUFFER_SIZE ) {
      // Two writes
      memcpy(&buffer[endIndex], &buf[0], dataSizeTillEnd); 
      memcpy(&buffer[0], 
             &buf[dataSizeTillEnd],
             unused - dataSizeTillEnd);
    } else {
      memcpy(&buffer[endIndex],
             &buf[0],
             unused);
    }
    count += unused;
    ret = unused;
  } else {
    // write len ammount
    if ( len > dataSizeTillEnd ) {
      // Two writes
      memcpy(&buffer[endIndex], &buf[0], dataSizeTillEnd);
      memcpy(&buffer[0], 
             &buf[dataSizeTillEnd],
             len - dataSizeTillEnd);
    } else {
      memcpy(&buffer[endIndex], &buf[0], len);
    }
    count += len;
    ret = len;
  }
  return ret;
}

//
// read_data is used to fill buf with len ammount of data from 
// the WAB. 
//
// Parameter:
//   buf - a pointer to a byte buffer to store the result
//   len - the number of bytes to read from the WAB
//
// Return: 
//   int - the number of successful bytes read from the WAB
//         and written to buf. 
//
int WrapAroundBuffer::read_data(char* buf, int len) {
  int bytesRead = 0;
  while (bytesRead < len && bytesRead < count) {
    buf[bytesRead++] = buffer[dataStart];
    dataStart = (dataStart+1) % WRAP_AROUND_BUFFER_SIZE;
    --count;
  }
  return bytesRead;
}

//
// used returns the number of bytes used in the Wrap Around Buffer.
// 
// Return:
//   int - # of bytes used by buffer.
int WrapAroundBuffer::used() {
  return ( count );
}

//
// unused returns the number of free bytes in the Wrap Around Buffer
//
// Return:
//   int - # of free bytes.
int WrapAroundBuffer::unused() {
  return ( WRAP_AROUND_BUFFER_SIZE - count );
}

//
// size returns the total # of bytes the buffer can store.
//
// Return:
//   int - returns the size of the buffer in bytes.
int WrapAroundBuffer::size() {
  return WRAP_AROUND_BUFFER_SIZE;
}
