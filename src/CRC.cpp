#include "CRC.h"
#include <string.h>


unsigned short mytcp::util::crc_algorithm(unsigned char * const data, int dLen, unsigned short remainder, int rLen) {
    int bit;
    int byte;
    //dLen = sizeof(*data);

    /* CRC byte by byte */
    for (byte = 0; byte < dLen; byte++) {
	remainder ^= data[byte] << 8; /* set remainder to current byte */

	for (bit = 8; bit > 0; bit--) { /*assuming 8 bits per byte*/
            /* if top bit set perform division */
	    if ( remainder & 0x8000 ) { /*if most significant bit set*/
                /*remainder=(remainder LEFTSHIFT 1) XOR CRC_16*/
		remainder = (remainder << 1) ^ CRC_16;
	    } else {
	        /*remainder= remainder LEFTSHIFT 1*/
		remainder = (remainder << 1);
	    }
	}
    }
    
    return remainder;
}


bool mytcp::util::crc_check(unsigned char* const data, int dLen, unsigned short remainder, int rLen) {
  bool returnVal = false;

  unsigned short result;

  unsigned char* d[dLen+ sizeof(short)];
  memcpy(d, data, dLen); //copy the data
  memcpy(&d[dLen], &remainder, sizeof(unsigned short)); //append remainder 
  unsigned char* dat = (unsigned char*)d;
  
  
  /*pass in crc and data to algorithm*/
  result = crc_algorithm(dat, dLen+sizeof(unsigned short), remainder, rLen);

  /*result returned should be zero*/
  if (result == 0x0000) {
	  returnVal = true;
  }

  return returnVal;
}

unsigned short mytcp::util::crc_generate(unsigned char * const data, int dLen, int rLen) {
  unsigned short remainder;
  remainder = 0x0000; /*zero out remainder*/

  remainder = crc_algorithm(data, dLen, remainder, rLen);

  return remainder;
}

